Uso del Widget Single Child view en una interfaz hecha en Flutter

Fue un tanto dificil hacerlo por la poca experiencia y el no poder 
encontrar el codigo fuente por parte de quien lo hizo (@whatsupcoder)

Se me hizo un reto, a pesar de conocer el idioma ingles, se hizo muy dificil
entender el acento Indú de la Dama que hizo el video. Pero así son las cosas,
si es dificil y lo logras, entonces aprendes. Gracias "@whatsupcoders"

Aqui el enlace al video:

https://www.youtube.com/watch?v=33KR07WEgFU

Aquí la Imagen de como quedó:

![Asi luce, con nuevos colores..!](assets/screenshot.png)

Todo esto hecho en flutter 1.9..!
# single_child_scroll_view_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
