import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class VistaEnScroll extends StatefulWidget {
  @override
  _VistaEnScrollState createState() => _VistaEnScrollState();
}

class _VistaEnScrollState extends State<VistaEnScroll> {
  Widget myChips(String chipName) {
    return Container(
      child: RaisedButton(
        color: Color.fromRGBO(214, 33, 150, 1),
        child: Text(
          chipName,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        onPressed: () {},
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
        )
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            FontAwesomeIcons.arrowLeft,
            color: Color.fromRGBO(214, 33, 150, 1),
          ),
          onPressed: () {
            //
          }
        ),
        title: Text('Eventos', style: TextStyle(color: Color.fromRGBO(214, 33, 150, 1),)),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              FontAwesomeIcons.search,
              color: Color.fromRGBO(214, 33, 150, 1),
            ),
            onPressed: () {
              //
            }
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: new Stack(
            children: <Widget>[
              new Positioned(// 9:00 pm
                child: Container(
                  color: Color.fromRGBO(56, 14, 127, 1),
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 34.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          child: Text(
                            'Hoy, 9:00 pm',
                            style: TextStyle(
                              color: Color.fromRGBO(228, 151, 205, 1), 
                              fontSize: 18.0,
                            ),
                          )
                        ),
                        Container(
                          child: Text(
                            'Yoga Suave y Facil',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 16.0, top: 8.0),
                          child: Container(
                            child: Wrap(
                              direction: Axis.horizontal,
                              spacing: 10.0,
                              runSpacing: 5.0,
                              children: <Widget>[
                                Center(
                                  child: myChips("Estas Anotado..!"),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              new Positioned( // 8:00 pm
                child: Container(
                  height: 480.0,
                  width: MediaQuery.of(context).size.width,
                  decoration: new BoxDecoration(
                    color: Color.fromRGBO(105, 21, 207, 1),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(75.0),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 16.0, bottom: 34.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          child: Text(
                            'Hoy, 8:00 pm',
                            style: TextStyle(
                              color: Color.fromRGBO(228, 151, 205, 1), 
                              fontSize: 18.0
                            ),
                          )
                        ),
                        Container(
                          child: Text(
                            'Practica de Frances, Ingles y Chino',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 60.0, top: 12.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            child: new Stack(
                              children: <Widget>[
                                Container(
                                  width: 180.0,
                                  child: Icon(
                                    FontAwesomeIcons.checkCircle,
                                    color: Color.fromRGBO(228, 151, 205, 1),
                                    size: 40.0,
                                  ),
                                ),
                                Container(
                                  width: 300.0,
                                  child: Icon(
                                    FontAwesomeIcons.timesCircle,
                                    color: Color.fromRGBO(228, 151, 205, 1),
                                    size: 40.0,
                                  ),
                                ),
                              ]
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              new Positioned( // 6:00 pm
                child: Container(
                  height: 300.0,
                  width: MediaQuery.of(context).size.width,
                  decoration: new BoxDecoration(
                    color: Color.fromRGBO(228, 151, 205, 1),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(75.0),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 18.0, bottom: 34.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          child: Text(
                            'Hoy, 6:00 pm',
                            style: TextStyle(
                              color: Color(0xFFC7166F), 
                              fontSize: 18.0
                            ),
                          )
                        ),
                        Container(
                          child: Text(
                            'Yoga y Meditación para Principiantes',                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 16.0, top: 8.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            child: new Stack(
                              children: <Widget>[
                                Container(
                                  width: 40.0,
                                  height: 40.0,
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: new DecorationImage(
                                      fit: BoxFit.fill,
                                      image: new AssetImage('assets/images/usuario_1.jpg'),
                                    ),
                                  ),
                                ),
                                new Positioned(
                                  left: 20.0,
                                  child: Container(
                                    width: 40.0,
                                    height: 40.0,
                                    decoration: new BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: new DecorationImage(
                                        fit: BoxFit.fill,
                                        image: new AssetImage('assets/images/usuario_5.jpg')
                                      ),
                                    ),
                                  ),
                                ),
                                new Positioned(
                                  left: 40.0,
                                  child: Container(
                                    width: 40.0,
                                    height: 40.0,
                                    decoration: new BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: new DecorationImage(
                                        fit: BoxFit.fill,
                                        image: new AssetImage('assets/images/usuario_1.jpg')
                                      ),
                                    ),
                                  ),
                                ),
                                new Positioned(
                                  left: 60.0,
                                  child: Container(
                                    width: 40.0,
                                    height: 40.0,
                                    decoration: new BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: new DecorationImage(
                                        fit: BoxFit.fill,
                                        image: new AssetImage('assets/images/usuario_5.jpg')
                                      ),
                                    ),
                                  ),
                                ),
                              ]
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              new Positioned( // Encabezado
                child: Container(
                  height: 120.0,
                  width: MediaQuery.of(context).size.width,
                  decoration: new BoxDecoration(
                    color: Color(0xFFFFFFFF),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(75.0),
                      bottomRight: Radius.circular(75.0),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.all(1),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50.0),
                          border: Border.all(
                            width: 2.0,
                            color: Color(0xFFC7166F),
                          ),
                        ),
                        child: Container(
                          width: 60.0,
                          height: 60.0,
                          child: Icon(
                            FontAwesomeIcons.music,
                            color: Color(0xFFC7166F)
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.all(1),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50.0),
                          border: Border.all(
                            width: 2.0,
                            color: Color(0xFFF49CC8),
                          ),
                        ),
                        child: Container(
                          width: 60.0,
                          height: 60.0,
                          child: Icon(
                            FontAwesomeIcons.chartLine,
                            color: Color(0xFFC7166F)
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.all(1),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50.0),
                          border: Border.all(
                            width: 2.0,
                            color: Color(0xFFF49CC8),
                          ),
                        ),
                        child: Container(
                          width: 60.0,
                          height: 60.0,
                          child: Icon(
                            FontAwesomeIcons.heart,
                            color: Color(0xFFC7166F),
                          ),
                        ),
                      ),
                    ]
                  ),
                ),
              ),
            ],
          )
        ),
      ),
    );
  }
}
