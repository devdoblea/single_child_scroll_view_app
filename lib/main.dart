import 'package:flutter/material.dart';
import 'package:single_child_scroll_view_app/vista_en_scroll.dart';

void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: VistaEnScroll(),
      ),
    );
  }
}